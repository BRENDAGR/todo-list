import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  title = 'todo-app';
  todoList = [''];
  todoItem = ''; 
  additem(){
    this.todoList.push(this.todoItem)
  }

  deleteitem(index: number){
    this.todoList.splice(index, 1);
  }
 
}
